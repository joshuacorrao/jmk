# Jimmy Mechanical Keyboard
### A CircuitPython mechanical keyboard firmware library
_When you just want to write some Python instead of doing something fancy_

### Bluetooth (BLE) features require the `adafruit_ble` library

## Current Status: IN PROGRESS
Inspired by KMK CircuitPython project, but written from scratch with fewer features for my personal use.

It currently only supports basic keyboard functionality and...
- split board via BLE (left side is always master)
- HID via BLE
- basic key layers
- 1 specific implementation of a Hold-Tap feature
- A basic Mod-Tap feature
- Combos (key number based)

## How to configure
- Import a keyboard config `run` (such as `jmk.config.example.run`) module and call `main()`
- Specific board configurations should be defined in jmk/config/{name}/
- See jmk/config/README.md for more configuration info
- jmk/config/example_usb for a simple wired keyboard
- jmk/config/joshua_split_ble for my personal configuration of a completely wireless split keyboard

## How to Extend
- In jmk/keys.py: Create any new Key types if needed
- In jmk/k.py: add any new key definitions

## Additional notes
- This keyboard currently assumes a columns-to-rows style matrix with diodes
- This is initially written for a Seeed Studio XIAO nRF52840 microcontroller board. It should be suitable for other nRF52840 boards, such as the "nice!nano" but hw_config.py needs to be changed to use different pins
- The BLEHelper seems to be more reliable when it is the first thing instantiated.
- `deferrable=True` keys found in the key queue will hold the key queue indefinitely
  - If `Key.process()` returns a `deferrable=False` key, the key queue will start flowing again
- `MatrixEvents` are updates from the matrix scanners, `KeyEvents` are `MatrixEvents` + `Key` objects
- The Combos system, hold all matrix events in a `combo_buffer` for a short period of time (16 ms default)
- Combos may be less reliable on the right hand due to event quantization caused by longer bluetooth connection intervals (5 ms vs 11.25)
- Are you really going to need it? You Ain't Gonna Need It!
