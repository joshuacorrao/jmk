from keypad import KeyMatrix
from keypad import Event as MatrixEvent

from jmk.manager import manager
from .base_queue import BaseQueue

try:
    from typing import TYPE_CHECKING, Optional
except ImportError:
    TYPE_CHECKING = None
    Optional = None
if TYPE_CHECKING:
    from keypad import KeyMatrix
    from jmk.keyboard import Keyboard


class MatrixEventQueue(BaseQueue):
    buffer_1: list[MatrixEvent] = []
    buffer_2: list[MatrixEvent] = []

    def __init__(self, keyboard: Keyboard, key_matrix: KeyMatrix):
        print("\nStarting Matrix event queue")
        self.keyboard: Keyboard = keyboard
        self.primary_matrix: KeyMatrix = key_matrix
        self.previous_queue = self.primary_matrix.events

    def update(self):
        matrix_event = self.primary_matrix.events.get()
        if matrix_event:
            self.buffer_2.append(matrix_event)
            manager.log_activity()

    def get(self) -> Optional[MatrixEvent]:
        """Get the latest MatrixEvent from the queue if any"""
        self.update()
        matrix_event = self.read_queue(self.buffer_2)
        return matrix_event


class UARTMatrixEventQueue(MatrixEventQueue):
    """Pull MatrixEvents from a KeyMatrix, and either send or receive them via an UART connection"""
    buffer_1: list[MatrixEvent] = []
    buffer_2: list[MatrixEvent] = []

    def __init__(self, keyboard, key_matrix):
        super().__init__(keyboard, key_matrix)
        self.offset = self.keyboard.hw_config.offset
        self.matrix_size = self.keyboard.hw_config.matrix_size
        self.uart = manager.uart

    def update(self):
        """Get MatrixEvent from primary KeyMatrix, if client, send them to host.
        If host receive additional MatrixEvent from client via uart_queue and add these to the main queue
        """
        super().update()

        if not self.uart:  # uart might be added later or removed and readded
            return
        if manager.is_host:
            self.receive_matrix_events()
            client_matrix_event = self.read_queue(self.buffer_1)
            if client_matrix_event:
                self.buffer_2.append(client_matrix_event)
        else:
            client_matrix_event = self.read_queue(self.buffer_2)
            if client_matrix_event:
                self.send_matrix_event(client_matrix_event)
        if client_matrix_event:
            manager.log_activity()

    @staticmethod
    def _serialize_matrix_event(update) -> bytearray:
        matrix_event_bytes = bytearray(2)
        matrix_event_bytes[0] = update.key_number
        matrix_event_bytes[1] = update.pressed
        return matrix_event_bytes

    def _deserialize_matrix_event(self, matrix_event_bytes: bytes) -> Optional[MatrixEvent]:
        key_number = matrix_event_bytes[0]
        pressed = matrix_event_bytes[1]
        if pressed > 1 or key_number > self.keyboard.hw_config.matrix_size:
            print(f"\nIgnoring deserialized matrix_event {key_number} > {self.matrix_size} , {pressed}")
            # this is not a valid value, assume this is a diagnostic value to be ignored
            return
        matrix_event = MatrixEvent(key_number=key_number+self.offset, pressed=bool(pressed))
        return matrix_event

    def send_matrix_event(self, matrix_event: MatrixEvent):
        """Send matrix event to UART"""
        matrix_event_bytes = self._serialize_matrix_event(matrix_event)
        if self.uart.write(matrix_event_bytes):
            self.print_event(matrix_event)
            manager.log_activity()
            return
        # write failed, pushing back into queue
        self.buffer_2.insert(0, matrix_event)

    def receive_matrix_events(self) -> None:
        """Receive MatrixEvents from UART and add them to the UART queue"""
        if self.uart is not None and self.uart.in_waiting:
            while self.uart.in_waiting >= 2:
                matrix_event_bytes = self.uart.read(2)
                matrix_event = self._deserialize_matrix_event(matrix_event_bytes)
                if matrix_event is not None:
                    self.buffer_1.append(matrix_event)
                    manager.log_activity()

    def print_event(self, matrix_event) -> None:
        if not manager.debug:
            return
        key_state_str = "+" if matrix_event.pressed else "-"
        if manager.debug_keys:
            print(f"{matrix_event.key_number}{key_state_str}", end=" ")
        else:
            print(key_state_str, end=" ")
