from jmk.manager import manager
from jmk.queues.base_queue import BaseQueue

try:
    from typing import TYPE_CHECKING, Optional
except ImportError:
    TYPE_CHECKING = None
    Optional = None
if TYPE_CHECKING:
    from jmk.keys import Key
    from jmk.keyboard import Keyboard
    from jmk.queues.combo_buffer import ComboBuffer
    from keypad import Event as MatrixEvent


class KeyEvent:
    deferred: bool = False

    def __init__(self, key: Key, matrix_event: MatrixEvent):
        self.key: Key = key
        self.matrix_event: MatrixEvent = matrix_event
        self.deferred: bool = key.deferrable

    def process(self):
        """Call the process method on this event's Key. if a key is returned, update this event's Key"""
        key = self.key.process(manager.keyboard, self.matrix_event)
        if key:
            self.key = key
            self.deferred = key.deferrable

    def print(self):
        if not manager.debug:
            return
        if manager.debug_keys:
            if self.matrix_event.pressed:
                output = self.key.press_label
            else:
                output = self.key.release_label
            print(output, end=" ")
        else:
            output = "+" if self.matrix_event.pressed else "-"
            print(output, end=" ")

    # TODO ADD A RECYCLE MATRIX EVENT METHOD

    def __str__(self):
        return f"{self.key} {self.matrix_event}"

    def __repr__(self):
        return f"{self.key} {self.matrix_event}"


class KeyEventQueue(BaseQueue):
    previous_queue: BaseQueue
    buffer_1: list[KeyEvent] = []
    buffer_2: list[KeyEvent] = []

    def __init__(self, keyboard: Keyboard, previous_queue: ComboBuffer):
        print("\nInitializing Key Event Queue")
        self.keyboard: Keyboard = keyboard
        self.previous_queue: ComboBuffer = previous_queue

    def update(self):
        matrix_event = self.previous_queue.get()
        if matrix_event:
            key = self.keyboard.get_key(matrix_event.key_number)
            key_event = KeyEvent(key, matrix_event)
            self.buffer_1.append(key_event)

        if not self.buffer_1 or self.buffer_1[0].deferred:
            pass  # do nothing
        else:
            out_key_event = self.buffer_1.pop(0)
            self.buffer_2.append(out_key_event)

        if self.buffer_1 and self.buffer_1[0].deferred:
            deferred_key_event = self.buffer_1[0]
            deferred_key_event.process()

    def get(self) -> Optional[KeyEvent]:
        """Get the latest KeyEvent from queue if any, hold if latest KeyEvent is deferred"""
        self.update()
        key_event = self.read_queue(self.buffer_2)
        if not key_event:
            return
        key_event.print()
        return key_event

    @staticmethod
    def read_queue(queue: list) -> KeyEvent:
        return super().read_queue(queue)
