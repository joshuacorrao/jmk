
class BaseQueue:
    previous_queue: object
    buffer_1: list
    buffer_2: list

    def update(self):
        raise NotImplementedError

    def get(self):
        raise NotImplementedError

    @staticmethod
    def read_queue(queue: list):
        """Pop first from list if any"""
        if queue:
            return queue.pop(0)

    def view_idx(self, idx: int):
        try:
            return self.buffer_1[idx]
        except IndexError:
            pass
