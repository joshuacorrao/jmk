from jmk.manager import manager
from jmk.utils import has_ts_expired
from .base_queue import BaseQueue
from jmk.keys import Combo

try:
    from typing import TYPE_CHECKING, Optional
except ImportError:
    TYPE_CHECKING = None
    Optional = None
if TYPE_CHECKING:
    from keypad import Event as MatrixEvent
    from jmk.keyboard import Keyboard


COMBO_TERM = 25


class ComboBuffer(BaseQueue):
    previous_queue: BaseQueue
    buffer_1: list[MatrixEvent] = []
    buffer_2: list[MatrixEvent] = []
    combo_map: dict[int, Combo]

    def __init__(self, keyboard: Keyboard, previous_queue: BaseQueue):
        print("\nInitializing Combo Buffer")
        self.keyboard: Keyboard = keyboard
        self.previous_queue: BaseQueue = previous_queue
        self.combo_map = {combo.value: combo for combo in self.keyboard.key_config.combos}

    def update(self):
        latest_matrix_event = self.keyboard.matrix_event_queue.get()
        if latest_matrix_event:
            self.buffer_1.append(latest_matrix_event)

        if not self.buffer_1:
            return

        now = manager.now
        oldest_event_ts = self.buffer_1[0].timestamp
        has_combo_check_expired = has_ts_expired(now, oldest_event_ts, COMBO_TERM)
        if not has_combo_check_expired:
            return

        combo_was_processed = self.maybe_process_combo()
        if combo_was_processed:
            return

        matrix_event = self.buffer_1.pop(0)
        self.buffer_2.append(matrix_event)

    def get(self) -> MatrixEvent:
        self.update()
        return self.read_queue(self.buffer_2)

    def maybe_process_combo(self) -> bool:
        if len(self.buffer_1) < 2 or not all(me.pressed for me in self.buffer_1):
            return False  # 1 key cannot be a combo. Only match pressed keys, ignore released

        key_numbers = tuple(matrix_event.key_number for matrix_event in self.buffer_1)
        combo_value = Combo.get_value(key_numbers)
        if combo_value in self.combo_map:
            combo = self.combo_map[combo_value]
            matrix_event = self.buffer_1[0]
            combo_was_processed = combo.process(self.keyboard, matrix_event)
            if combo_was_processed:
                self.buffer_1.clear()
                return True

    @staticmethod
    def read_queue(queue: list) -> MatrixEvent:
        return super().read_queue(queue)

