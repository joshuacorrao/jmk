from micropython import const
import alarm

try:
    from typing import TYPE_CHECKING
except ImportError:
    TYPE_CHECKING = None
if TYPE_CHECKING:
    import microcontroller


def read_queue(queue: list):
    """Pop first from list if any"""
    if queue:
        return queue.pop(0)


_TICKS_PERIOD = const(1 << 29)
_TICKS_MAX = const(_TICKS_PERIOD - 1)
_TICKS_HALFPERIOD = const(_TICKS_PERIOD // 2)


def ticks_add(ticks: int, delta: int) -> int:
    """Add a delta to a base number of ticks, performing wraparound at 2**29ms."""
    return (ticks + delta) % _TICKS_PERIOD


def ticks_diff(ticks1: int, ticks2: int) -> int:
    """Compute the signed difference between two ticks values, assuming that they are within 2**28 ticks"""
    diff = (ticks1 - ticks2) & _TICKS_MAX
    diff = ((diff + _TICKS_HALFPERIOD) & _TICKS_MAX) - _TICKS_HALFPERIOD
    return diff


def ticks_less(ticks1: int, ticks2: int) -> bool:
    """Return true if ticks1 is less than ticks2, assuming that they are within 2**28 ticks"""
    return ticks_diff(ticks1, ticks2) < 0


def has_ts_expired(latest_ts: int, initial_ts: int, term_ms: int) -> bool:
    """True if latest_ts - initial_ts difference is less than ms"""
    deadline_ts = initial_ts + term_ms
    return ticks_less(deadline_ts, latest_ts)


def deep_sleep(col_pins: tuple[microcontroller.Pin], row_pins: tuple[microcontroller.Pin]):
    """Set pin alarms to wake up on any change"""
    print("\nDEEP SLEEP START")
    col_pin_alarms = [
        alarm.pin.PinAlarm(pin=pin_id, value=True, pull=False)
        for pin_id in col_pins
    ]
    row_pin_alarms = [
        alarm.pin.PinAlarm(pin=pin_id, value=False, pull=True)
        for pin_id in row_pins
    ]
    pin_alarms = col_pin_alarms + row_pin_alarms
    alarm.exit_and_deep_sleep_until_alarms(*pin_alarms)
