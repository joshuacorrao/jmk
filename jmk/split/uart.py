try:
    from typing import TYPE_CHECKING, Optional
except ImportError:
    TYPE_CHECKING = None
    Optional = None
if TYPE_CHECKING:
    from adafruit_ble.services.nordic import UARTService


class UART:
    in_waiting: None
    is_host: bool
    uart_service: UARTService

    def write(self, data):
        raise NotImplementedError

    def read(self, count):
        raise NotImplementedError
