from jmk.manager import manager
from .uart import UART


try:
    from typing import TYPE_CHECKING, Optional
except ImportError:
    TYPE_CHECKING = None
    Optional = None
if TYPE_CHECKING:
    from adafruit_ble.services.nordic import UARTService
    from jmk.ble.base import BLEHelperBase


class BLEUART(UART):
    """Facade for UARTService with connection checking"""

    def __init__(self, helper: BLEHelperBase, uart: UARTService):
        print("\nBuilding new UART controller")
        self.helper: BLEHelperBase = helper
        self.uart_service: UARTService = uart

    @property
    def is_host(self) -> bool:
        return self.helper.is_host

    @property
    def in_waiting(self) -> int:
        return self.uart_service.in_waiting

    def read(self, length: int) -> bytes:
        if not manager.check_connections():
            return b'\x90\x90'
        return self.uart_service.read(length)

    def write(self, message: bytes) -> bool:
        if not manager.check_connections():
            return False
        try:
            self.uart_service.write(message)
            return True
        except Exception as e:
            print("")
            print(e)
            print("\nUART write error")
        try:
            self.helper.client_conn.disconnect()
            print("\nConnection disconnected")
        except Exception as e:
            print("")
            print(e)
            print("\nDisconnect failed")
        self.helper.client_conn = None
        return False
