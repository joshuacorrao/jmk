"""
Load and run this first thing before importing or initializing any other keyboard components
BLEHelper

Negotiates BLE connection between host and client,
validates connection,
advertise BLE HID if one is provided
"""

from micropython import const
from supervisor import ticks_ms

from adafruit_ble import Advertisement
from adafruit_ble.services.nordic import UARTService
from adafruit_ble.advertising.standard import ProvideServicesAdvertisement

from jmk.manager import manager
from jmk.ble.base import BLEHelperBase
from jmk.split.ble import BLEUART

try:
    from typing import Optional
except ImportError:
    Optional = None


BLE_APPEARANCE_HID_KEYBOARD = const(961)


class BLEHelperHost(BLEHelperBase):
    def __init__(self, name, is_host, pair_address_bytes=None):
        super().__init__(name=name, is_host=is_host, pair_address_bytes=pair_address_bytes)

        self.advertisement: Optional[Advertisement] = None
        self.hid_advertisement: Optional[Advertisement] = None
        uart = UARTService()
        manager.uart = BLEUART(self, uart)
        if manager.hw_config.hid_ble:
            manager.led_helper.red_on()

    """Connect two devices together via BLE UART, Host and Client, hold connections in memory"""
    def check_connections(self) -> bool:
        """Validates the correct number of BLE connections"""
        connection_count = self.get_connection_count()
        if not self.is_client_connected():
            self.missing_connections = True
            if connection_count == 1:
                # other connection is probably HID
                self.status = self.ConnectionStatus.NO_CLIENT
            else:
                self.status = self.ConnectionStatus.NO_CONNECTIONS
            return False
        if connection_count == 2:
            manager.led_helper.red_off()
            self.missing_connections = False
            self.status = self.ConnectionStatus.CONNECTED_2
            return True
        if connection_count == self.expected_conn_count:
            self.missing_connections = False
            self.status = self.ConnectionStatus.CONNECTED
            return True
        # probably an issue regarding HID
        if manager.hw_config.hid_ble:
            if self.hid is None and manager.keyboard.hid is not None:
                self.hid = manager.keyboard.hid
            # HID is configured but it is not connected
            self.missing_connections = True
            self.status = self.ConnectionStatus.NO_HID
            manager.led_helper.red_blink()
            self.advertise_hid()
            return True  # HID is non essential during connection check
        self.missing_connections = True
        self.status = self.ConnectionStatus.UNKNOWN

    def connect(self):
        if self.advertisement is None:
            self.advertise_host()
        return self.await_client()

    def advertise_host(self):
        """Start advertising with UART service for client to connect to"""
        self.radio.stop_advertising()
        self.advertisement = ProvideServicesAdvertisement(manager.uart.uart_service)
        self.radio.name = self.name
        print(f"\nHost is Advertising - {self.radio._adapter.address}")
        self.radio.start_advertising(self.advertisement)

    def await_client(self) -> bool:
        if self.is_scan_expired:
            print("\nClient did not connect")
            self.radio.stop_advertising()
            self.advertisement = None
            self.last_scan_ts = ticks_ms()
            return False

        if self.is_client_connected():
            if not self.wait_for_client_message():
                print("\nNo message received from client")
                self.radio.stop_advertising()
                self.advertisement = None
                return False
            print("\nClient has connected")
            self.radio.stop_advertising()
            self.advertisement = None
            return True

        return False

    def is_client_connected(self) -> bool:
        """Attempt to quickly check if client is connected before slow validation"""
        connection_count = self.get_connection_count()
        if connection_count == 0:
            return False
        if self.client_conn is not None and self.client_conn.connected and self.client_conn in self.radio.connections:
            return True
        if connection_count == self.expected_conn_count:
            return True
        if connection_count != self.expected_conn_count:
            return self.validate_client_connection()
        self.client_conn = None
        print("\nClient is not connected")
        return False

    def validate_client_connection(self) -> bool:
        """Check the client connection's services, this is not fast"""
        # TODO it would be great to do this another way
        for connection in self.radio.connections:
            bleio_connection = connection._bleio_connection
            connection_services = bleio_connection.discover_remote_services()
            for service in connection_services:
                if str(service.uuid).startswith("UUID('adaf0001"):
                    self.client_conn = connection
                    print("\nClient connection validated")
                    return True
        return False

    def wait_for_client_message(self) -> bool:
        print("\nWaiting for data from client")
        while manager.uart.uart_service.in_waiting < 2:
            if self.is_scan_expired:
                print("\nNo message received from client")
                return False
        data = manager.uart.uart_service.read(2)
        print(f"\nMessage Received {data}")
        return True

    def advertise_hid(self) -> None:
        if self.radio.advertising:
            print(f"\nAdvertising HID service {self.radio.name}")
            return
        advertisement = ProvideServicesAdvertisement(self.hid.hid_service)
        advertisement.appearance = BLE_APPEARANCE_HID_KEYBOARD
        self.radio.start_advertising(advertisement)
        self.hid_advertisement = advertisement
        print(f"\nAdvertising HID service {self.radio.name}")
