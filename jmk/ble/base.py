"""
Load and run this first thing before importing or initializing any other keyboard components
BLEHelper

Negotiates BLE connection between host and client,
validates connection,
advertise BLE HID if one is provided

BLEUART acts as a facade for UART so the keyboard doesn't need to concern itself with connection errors
"""
from supervisor import ticks_ms
import _bleio

from adafruit_ble import BLERadio

from jmk.manager import manager
from jmk.utils import has_ts_expired
from jmk.split.ble import BLEUART

try:
    from typing import TYPE_CHECKING, Optional
except ImportError:
    TYPE_CHECKING = None
    Optional = None
if TYPE_CHECKING:
    from adafruit_ble import BLEConnection


class BLEHelperBase:
    """Connect two devices together via BLE UART, Host and Client, hold connections in memory"""

    class ConnectionStatus:
        NO_CONNECTIONS = "NO CONNECTIONS"
        NO_CLIENT = "CLIENT IS NOT CONNECTED"
        NO_HID = "HID IS NOT CONNECTED"
        CONNECTED = "CONNECTED"
        CONNECTED_2 = "CONNECTED 2"
        NO_HOST = "NOT CONNECTED TO HOST"
        UNKNOWN = "UNKNOWN CONNECTION STATUS"

    def __init__(self, name: str, is_host: bool, pair_address_bytes: bytes = None):
        self.name = name
        self.is_host = is_host
        self.hid = None
        self.pair_address = _bleio.Address(pair_address_bytes, 1)

        self.uart: Optional[BLEUART] = None
        self.client_conn: Optional[BLEConnection] = None

        self.last_scan_ts: int = ticks_ms()  # used to reset advertisements and scans

        self.radio: BLERadio = BLERadio()
        self.radio.name = name
        self.radio.stop_scan()
        self.radio.stop_advertising()

        manager.ble_helper = self
        manager.led_helper.blue_blink()
        manager.is_host = is_host
        self.status: str = self.ConnectionStatus.NO_CONNECTIONS
        self.missing_connections = True

    def get_connection_count(self) -> int:
        return len(self.radio.connections)

    @property
    def expected_conn_count(self) -> int:
        if manager.hw_config.hid_ble:
            return 2
        return 1

    @property
    def is_scan_expired(self) -> bool:
        now = ticks_ms()
        return has_ts_expired(now, self.last_scan_ts, 20000)

    def clear_bonding(self):
        self.radio._adapter.erase_bonding()

    def check_connections(self) -> bool:
        raise NotImplementedError

    def connect(self) -> bool:
        raise NotImplementedError
