from micropython import const

from adafruit_ble.services.nordic import UARTService
from adafruit_ble.advertising.standard import ProvideServicesAdvertisement

from jmk.manager import manager
from jmk.ble.base import BLEHelperBase
from jmk.split.ble import BLEUART

try:
    from typing import TYPE_CHECKING, Optional
except ImportError:
    TYPE_CHECKING = None
    Optional = None
if TYPE_CHECKING:
    from adafruit_ble import BLEConnection
    from adafruit_ble.services import Service


BLE_APPEARANCE_HID_KEYBOARD = const(961)


class BLEHelperClient(BLEHelperBase):
    """Connect two devices together via BLE UART, Host and Client, hold connections in memory"""

    def connect(self) -> bool:
        if self.check_connections() and self.check_uart():
            return True
        if self.pair_address:
            return self.direct_connect()
        return self.find_and_connect_to_host()

    def direct_connect(self) -> bool:
        print(f"\nAttempting direct connection {self.pair_address}")
        try:
            print("\nConnecting to configured address")
            client_conn = self.radio.connect(self.pair_address)
            print("\nInitial connection success")
            print(f"\n{client_conn.connection_interval=}")
            self.attempt_client_uart(client_conn)
            client_conn.connection_interval = 11.25
            print(f"\n{client_conn.connection_interval=}")
            self.client_conn = client_conn
            return True
        except Exception as e:
            print("")
            print(e)
            print("Direct Connection failed")
            self.radio._adapter.erase_bonding()
        return False

    def attempt_client_uart(self, client_conn: BLEConnection) -> None:
        print("\nAttempting to connect UART")
        _uart: UARTService | Service = client_conn[UARTService]
        print("\nConfiguration local UART")
        print("\nLocal UART opened")
        self.send_test_bytes(_uart, 2)
        print("\nUART connected")

        if self.uart is None:
            self.uart = BLEUART(self, _uart)
        else:
            self.uart.uart_service = _uart
        if manager.uart is None:
            manager.uart = self.uart

    def send_test_bytes(self, _uart: UARTService, count):
        """Send a number of test bytes to the host"""
        test_message = bytearray(2)
        for idx in range(count):
            test_message = bytearray(2)
            test_message[idx] = 128  # this value should be ignored by the host keyboard
        _uart.write(test_message)

    def check_uart(self):
        """Used for validating existing uart is working."""
        if self.uart and self.uart.uart_service:
            try:
                self.send_test_bytes(self.uart.uart_service, 2)
                return True
            except Exception as e:
                print("")
                print(e)
        return False

    def find_and_connect_to_host(self) -> bool:
        scanner = self.radio.start_scan(ProvideServicesAdvertisement, timeout=20)
        for advert in scanner:
            print(advert)
            print(advert.rssi)
            if UARTService in advert.services and advert.rssi > -70:
                print("\nUART FOUND")
                try:
                    client_conn = self.radio.connect(advert)
                    self.attempt_client_uart(client_conn)
                except Exception as e:
                    print("")
                    print(e)
                    print("\nTrying again")
                    self.client_conn = None
                    self.radio.stop_scan()
                    return False
                print("\nCONNECTED")
                self.client_conn = client_conn
                self.radio.stop_scan()
                return True
        print("\nScan timed out")
        self.radio.stop_scan()

    def check_connections(self) -> bool:
        """Validates the correct number of BLE connections"""
        connection_count = len(self.radio.connections)
        if connection_count == 1:
            self.status = self.ConnectionStatus.CONNECTED
            return True
        self.status = self.ConnectionStatus.NO_HOST
        return False
