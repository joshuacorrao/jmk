from storage import getmount
from binascii import unhexlify


def create_split_ble():
    """Get a configured BLEHelper depending on the storage name"""
    # just use the mount name for now to determine BLE config

    name = str(getmount("/").label)
    if name.endswith("L"):
        is_host = True
        print("\nConfiguring split BLE as Left side (HOST)")
    elif name.endswith('R'):
        print("\nConfiguring split BLE as Right side (CLIENT)")
        is_host = False
    else:
        raise ValueError("Invalid drive name")

    try:
        with open("pair_address.txt") as f:
            pair_address = f.readline().encode()
            pair_address_bytes = unhexlify(pair_address)
    except Exception as e:
        print(e)
        print("\nNo pair address found on storage")
        pair_address_bytes = None

    if is_host:
        from .host import BLEHelperHost as BLEHelper
    else:
        from .client import BLEHelperClient as BLEHelper
    ble_helper = BLEHelper(name=name, is_host=is_host, pair_address_bytes=pair_address_bytes)
    return ble_helper
