from digitalio import DigitalInOut
try:
    from typing import Optional
except ImportError:
    Optional = None


class LEDHelper:
    red_io: Optional[DigitalInOut]
    green_io: Optional[DigitalInOut]
    blue_io: Optional[DigitalInOut]
    _red_value: Optional[bool]
    _green_value: Optional[bool]
    _blue_value: Optional[bool]

    def __init__(self):
        self.red_io = None
        self.green_io = None
        self.blue_io = None
        self._red_value = True
        self._green_value = True
        self._blue_value = True

    def update(self):
        if self.red_io is not None:
            if self._red_value is None:  # Blink
                self.red_io.value = not self.red_io.value
            else:
                self.red_io.value = self._red_value
        if self.green_io is not None:
            self.green_io.value = self._green_value
            if self._green_value is None:  # Blink
                self.green_io.value = not self.green_io.value
            else:
                self.green_io.value = self._green_value
        if self.blue_io is not None:
            if self._blue_value is None:  # Blink
                self.blue_io.value = not self.blue_io.value
            else:
                self.blue_io.value = self._blue_value

    def init_red(self, red_pin):
        if not red_pin:
            return
        self.red_io = DigitalInOut(red_pin)
        self.red_io.switch_to_output()
        self.red_io.value = self._red_value

    def init_green(self, green_pin):
        if not green_pin:
            return
        self.green_io = DigitalInOut(green_pin)
        self.green_io.switch_to_output()
        self.green_io.value = self._green_value

    def init_blue(self, blue_pin):
        if not blue_pin:
            return
        self.blue_io = DigitalInOut(blue_pin)
        self.blue_io.switch_to_output()
        self.blue_io.value = self._blue_value

    def red_on(self):
        self._red_value = False

    def red_blink(self):
        self._red_value = None

    def red_off(self):
        self._red_value = True

    def green_on(self):
        self._green_value = False

    def green_off(self):
        self._green_value = True

    def blue_on(self):
        self._blue_value = False

    def blue_blink(self):
        self._blue_value = None

    def blue_off(self):
        self._blue_value = True
