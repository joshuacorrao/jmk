try:
    from typing import TYPE_CHECKING, Self
except ImportError:
    TYPE_CHECKING = None
    Self = None
if TYPE_CHECKING:
    from keypad import Event as MatrixEvent
    from jmk.keyboard import Keyboard


class Key:
    """Maybe store an HID code, do something when Key.process() is called"""
    code: int
    label: str
    string: str
    press_label: str
    release_label: str
    modifiers: tuple[int]

    deferrable = False  # Will tell the Key Event queue to hold and process

    def __init__(self, code: int = 0, label: str = "", modifiers: tuple[int, ...] = None, string: str = None):
        self.code = code
        self.label = label
        if string is None:
            string = self.label
        self.string = string
        self.press_label = f"{label}+"
        self.release_label = f"{label}-"
        self.modifiers = modifiers or tuple()
        key_registry.register_key(self)

    def process(self, keyboard: Keyboard, matrix_event: MatrixEvent) -> Self:
        """Add and remove the key to pressed keys, at the MatrixEvent key_number index"""
        if matrix_event.pressed:
            keyboard.pressed_keys[matrix_event.key_number] = self
        else:
            keyboard.pressed_keys.pop(matrix_event.key_number, None)
        return self

    def __str__(self) -> str:
        return self.label

    def __repr__(self) -> str:
        return self.label


class KeyRegistry:
    code_map = {}
    string_map = {}

    def register_key(self, key: Key) -> None:
        if key.code or key.modifiers:
            self.code_map[(key.code, key.modifiers)] = key
        if len(key.string) == 1:
            self.string_map[key.string] = key


key_registry = KeyRegistry()


class NullKey(Key):
    def process(self, keyboard: Keyboard, matrix_event: MatrixEvent) -> Key:
        return self


# Useful for clearing out a deferred KeyEvent in the queue.
# Swapping a deferred KeyEvent's key to this will un-defer the KeyEvent but will do nothing when processed
null_key = NullKey(label="<NULL>")
empty_key = Key(label="<EMPTY>")
