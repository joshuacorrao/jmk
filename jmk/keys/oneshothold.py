from jmk.manager import manager
from .base import Key

try:
    from typing import TYPE_CHECKING, Self
except ImportError:
    TYPE_CHECKING = None
    Self = None
if TYPE_CHECKING:
    from keypad import Event as MatrixEvent
    from jmk.keyboard import Keyboard


class OneShotHoldKey(Key):
    """Hold another key for the duration of the next keypress"""
    deferrable = True
    hold_key: Key

    def __init__(self, hold_key: Key, label: str):
        super().__init__(label=label)
        self.hold_key = hold_key

    def process(self, keyboard: Keyboard, matrix_event: MatrixEvent) -> Key:
        if matrix_event.pressed and self.hold_key not in keyboard.pressed_keys.values():
            keyboard.pressed_keys[matrix_event.key_number] = self.hold_key

        return_key = self
        next_key_event = keyboard.key_event_queue.view_idx(1)

        if next_key_event is None:
            return return_key

        if matrix_event.pressed:
            if next_key_event.key != self:
                # interrupted by a different key, no longer a oneshot
                return self.hold_key

            elif next_key_event.matrix_event.released:
                manager.led_helper.green_off()
                # interrupted by self release. one shot time!!!
                third_key_event = keyboard.key_event_queue.view_idx(2)
                if third_key_event is not None and third_key_event.matrix_event.pressed:
                    # second press has happened
                    # swap self release with this next press
                    kevq = keyboard.key_event_queue.buffer_1 # just to shorten the next swap
                    kevq[1], kevq[2] = kevq[2], kevq[1]
                    return_key = self.hold_key
                    manager.led_helper.green_on()

        elif next_key_event.matrix_event.released:
            # make this release happen after the next one
            return_key = next_key_event.key
            next_key_event.key = self.hold_key

        return return_key
