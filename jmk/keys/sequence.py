from jmk.manager import manager
from jmk.utils import has_ts_expired
from .base import Key, null_key, key_registry

try:
    from typing import TYPE_CHECKING, Self, Optional
except ImportError:
    TYPE_CHECKING = None
    Self = None
    Optional = None
if TYPE_CHECKING:
    from keypad import Event as MatrixEvent
    from jmk.keyboard import Keyboard


class SequenceKey(Key):
    """Place a number of key events in the key event queue when pressed"""
    deferrable = True
    interval: int = 40
    sequence_counter: Optional[int] = None
    sequence: str
    label: str

    def __init__(self, sequence: str, label: str):
        super().__init__(label=label)
        self.sequence = sequence
        self.length = len(sequence)
        self.last_ts = manager.now

    def process(self, keyboard: Keyboard, matrix_event: MatrixEvent) -> Key:
        if self.sequence_counter is None:
            self.sequence_counter = 0
            self.last_ts = manager.now
            return self
        elif self.sequence_counter == self.length:
            self.sequence_counter = None
            return null_key
        else:
            if has_ts_expired(manager.now, self.last_ts, self.interval):
                keyboard.pressed_keys[matrix_event.key_number] = null_key
                self.last_ts = manager.now
                self.sequence_counter += 1
            elif keyboard.pressed_keys.get(matrix_event.key_number) is null_key:
                keyboard.pressed_keys.pop(matrix_event.key_number, None)
            else:
                s = self.sequence[self.sequence_counter]
                key = key_registry.string_map[s]
                keyboard.pressed_keys[matrix_event.key_number] = key
            return self
