from keypad import Event as MatrixEvent

from jmk.manager import manager
from jmk.utils import has_ts_expired
from .base import Key, null_key

try:
    from typing import TYPE_CHECKING, Self
except ImportError:
    TYPE_CHECKING = None
    Self = None
if TYPE_CHECKING:
    from jmk.keyboard import Keyboard


class LayerHoldKey(Key):
    press_layer: int
    release_layer: int

    def __init__(self, press_layer: int, release_layer: int, label: str):
        super().__init__(label=label)
        self.press_layer = press_layer
        self.release_layer = release_layer

    def process(self, keyboard: Keyboard, matrix_event: MatrixEvent) -> Key:
        """Swap the keyboard's active layer, add this key to other_keys for reference"""
        if matrix_event.pressed:
            manager.led_helper.green_off()
            keyboard.keys = keyboard.key_config.layers[self.press_layer]
            keyboard.other_keys[matrix_event.key_number] = self
        else:
            keyboard.keys = keyboard.key_config.layers[self.release_layer]
            keyboard.other_keys.pop(matrix_event.key_number, None)
            manager.led_helper.green_on()
        return self


class LayerHoldTapKey(Key):
    """HoldTap specifically designed for switching layers or tapping a key

    Determined on this keys press time 'hold_term'. Will send tap key if no other keys were pressed while holding
    Will not tap a key if held for longer than 'no_tap_hold_term'
    """
    deferrable = True
    hold_term = 175
    no_tap_hold_term = 750
    tap_key: Key
    pressed: bool
    held: bool

    def __init__(self, press_layer: int, release_layer: int, tap_key: Key, label: str):
        super().__init__(label=label)
        self.tap_key = tap_key
        self.press_layer = press_layer
        self.release_layer = release_layer
        self.pressed = False
        self.held = False

    def process(self, keyboard: Keyboard, matrix_event: MatrixEvent) -> Key:
        # Reminder that returning
        if matrix_event.released:
            if self.held:
                self.held = False
                print(f"LHT: {self.label}-", end=" ")
            self.pressed = False
            keyboard.change_layer(self.release_layer)
            keyboard.other_keys.pop(matrix_event.key_number, None)
            manager.led_helper.green_on()
            return self.tap_key

        else:  # Pressed
            if not self.pressed:
                self.pressed = True
                print(f"LHT: {self.label}+", end=" ")

            next_key_event = keyboard.key_event_queue.view_idx(1)
            if next_key_event is None:
                return self  # Keep waiting

            # Next key event detected, time to resolve...

            if next_key_event.key is self:
                if has_ts_expired(manager.now, matrix_event.timestamp, self.no_tap_hold_term):
                    return null_key
                return self.tap_key

            if (  # this prefers tap to layer hold
                not has_ts_expired(manager.now, matrix_event.timestamp, self.hold_term)
                or next_key_event.matrix_event.released
            ):
                return self.tap_key

            # Conditions to switch to a layer have been met.
            print(f"LAYER {self.press_layer}", end=" ")

            self.held = True
            manager.led_helper.green_off()
            keyboard.change_layer(self.press_layer)
            keyboard.other_keys[matrix_event.key_number] = self
            if next_key_event.matrix_event.pressed:
                new_next_key = keyboard.get_key(next_key_event.matrix_event.key_number)
                print(f"{next_key_event.key}+ >> {new_next_key}+", end=" ")
                next_key_event.key = new_next_key

            return null_key
