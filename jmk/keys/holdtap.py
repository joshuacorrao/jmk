from jmk.manager import manager
from jmk.utils import has_ts_expired
from .base import Key

try:
    from typing import TYPE_CHECKING, Self
except ImportError:
    TYPE_CHECKING = None
    Self = None
if TYPE_CHECKING:
    from keypad import Event as MatrixEvent
    from jmk.keyboard import Keyboard


class HoldTapKey(Key):
    deferrable = True
    hold_term: int = 175
    hold_key: Key
    tap_key: Key

    def __init__(self, hold_key: Key, tap_key: Key, label: str):
        super().__init__(label=label)
        self.hold_key = hold_key
        self.tap_key = tap_key

    def process(self, keyboard: Keyboard, matrix_event: MatrixEvent) -> Key:
        """Reference the newest possible KeyEvent, return either self, hold_key, or tap_key depending on timing"""

        next_key_event = keyboard.key_event_queue.view_idx(1)

        return_key = self
        if matrix_event.released:
            return_key = self.tap_key

        if next_key_event is not None:
            new_matrix_event = next_key_event.matrix_event
            if has_ts_expired(new_matrix_event.timestamp, matrix_event.timestamp, self.hold_term):
                return_key = self.hold_key
            else:
                return_key = self.tap_key
        else:
            if has_ts_expired(manager.now, matrix_event.timestamp, self.hold_term):
                return_key = self.hold_key

        return return_key
