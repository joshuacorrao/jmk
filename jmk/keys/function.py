from .base import Key


try:
    from typing import TYPE_CHECKING, Self
except ImportError:
    TYPE_CHECKING = None
    Self = None
if TYPE_CHECKING:
    from keypad import Event as MatrixEvent
    from jmk.keyboard import Keyboard


class FunctionKey(Key):
    """Run a provided functions on press and release"""
    press_func: callable
    release_func: callable
    deferrable: bool

    def __init__(self, press_func: callable, release_func: callable = None, deferrable: bool = False, label: str = ""):
        super().__init__(label=label)
        self.press_func = press_func
        self.release_func = release_func
        self.deferrable = deferrable

    def process(self, keyboard: Keyboard, matrix_event: MatrixEvent) -> Key:
        if matrix_event.pressed:
            return self.press_func(self, keyboard, matrix_event)
        elif self.release_func is not None:
            return self.release_func(self, keyboard, matrix_event)
