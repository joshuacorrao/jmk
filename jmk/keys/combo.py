from keypad import Event as MatrixEvent

from jmk.manager import manager
from .base import empty_key

try:
    from typing import TYPE_CHECKING, Self
except ImportError:
    TYPE_CHECKING = None
    Self = None
if TYPE_CHECKING:
    from jmk.keyboard import Keyboard
    from .base import Key


class Combo:
    """Special handling for combos that activate keys"""

    combo_key: Key
    _key_numbers: tuple[int]
    _hash: int
    label: str
    value: int

    def __init__(self, combo_key: Key, key_numbers: tuple):
        if len(key_numbers) < 2:
            raise ValueError("Combo objects must have more than 1 number in key_numbers.")
        self.combo_key = combo_key
        self._key_numbers = key_numbers
        self.value = self.get_value(self._key_numbers)
        self._hash = hash(frozenset(key_numbers))
        self.label = f"{self.combo_key} {self.key_numbers}"

    @property
    def key_numbers(self):
        return self._key_numbers

    def process(self, keyboard: Keyboard, matrix_event: MatrixEvent = None) -> bool:
        if matrix_event.pressed:
            new_matrix_event = MatrixEvent(key_number=self.key_numbers[0], pressed=True, timestamp=manager.now)
            if manager.debug and manager.debug_keys:
                print(f"\nCOMBO! {self} ({new_matrix_event.key_number})", end=" ")
            elif manager.debug:
                print("COMBO!", end=" ")
            for key_number in self.key_numbers:
                keyboard.pressed_keys[key_number] = empty_key
            self.combo_key.process(keyboard, new_matrix_event)
            return True

        if matrix_event.released:
            print("are we getting here?")
            keyboard.pressed_keys.pop(matrix_event.key_number, None)
            print(keyboard.pressed_keys)
            return False

    @staticmethod
    def get_value(key_numbers: tuple[int]) -> int:
        val = 0
        for number in key_numbers:
            key_bits = 1 << number
            val = val | key_bits
        return val

    def __hash__(self) -> int:
        return self._hash

    def __eq__(self, other) -> bool:
        if isinstance(other, (set, frozenset, self.__class__)):
            number_set = set(self._key_numbers)
            return number_set == other
        return False

    def __str__(self) -> str:
        return self.label

    def __repr__(self) -> str:
        return self.label
