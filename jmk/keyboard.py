from jmk.manager import manager

from keypad import KeyMatrix
from jmk.hid import KeyboardHID, DummyHID
from jmk.queues.combo_buffer import ComboBuffer
from jmk.queues.key_event import KeyEventQueue
from jmk.queues.scanner import UARTMatrixEventQueue
from jmk.queues.scanner import MatrixEventQueue

try:
    from typing import TYPE_CHECKING
except ImportError:
    TYPE_CHECKING = None
if TYPE_CHECKING:
    from jmk.keys import Key
    from jmk.config import HardwareConfig, KeyConfig


class Keyboard:
    """Store Keyboard config, event queues, pressed keys, update() cycles the state

    A matrix scanner needs to populate the Keyboard.pressed_keys dictionary
    MatrixEventQueue.get() > KeyEventQueue.get() > KeyEvent.process() > Key.process() > Keyboard.pressed_keys

    The HID reads the Keyboard.pressed_keys and sends an HID report to the computer
    KeyboardHID.update(Keyboard.pressed_keys) > computer
    """

    def __init__(self, hw_config: HardwareConfig, key_config: KeyConfig):
        print("\nInitializing Keyboard")
        self.hw_config = hw_config
        self.key_config = key_config
        self.hid_constructor = KeyboardHID.get_usb_hid
        self.hid: KeyboardHID = None
        self.split: bool = False
        self.use_ble: bool = False
        self.uart = None
        self.key_matrix: KeyMatrix = None
        self.matrix_event_queue_cls = MatrixEventQueue
        self.matrix_event_queue: MatrixEventQueue = None
        self.combo_buffer: ComboBuffer = None
        self.key_event_queue: KeyEventQueue = None
        self.ble_helper = None
        self.offset: int = 0
        self.is_host: bool = manager.is_host

        self.keys: tuple[Key] = self.key_config.keys
        self.layer: int = 0
        self.pressed_keys: dict[int, Key] = dict()
        self.other_keys: dict[int, Key] = dict()  # pressed keys that won't be sent to HID

        manager.keyboard = self

    def change_layer(self, layer: int) -> None:
        new_layer_keys = self.key_config.layers[layer]
        self.keys = new_layer_keys
        self.layer = layer

    def get_key(self, key_number: int) -> Key:
        """Get the appropriate Key for a key_number, considering the current state, look at pressed keys first"""
        key = self.pressed_keys.get(key_number)
        if key:
            return key
        key = self.other_keys.get(key_number)
        if key:
            return key
        key_coord = self.hw_config.inverse_coords[key_number]
        key = self.keys[key_coord]
        return key

    def update(self):
        """Get the latest state of the keyboard. Send changes to HID if any"""
        key_event = self.key_event_queue.get()
        if key_event:
            key_event.process()
            # TODO recycle matrix events here
        if self.pressed_keys or key_event:
            pressed_keys = list(self.pressed_keys.values())
            self.hid.update(pressed_keys)

    def build(self):
        if self.hw_config.hid_ble:
            self.use_ble = True
            self.hid_constructor = KeyboardHID.get_ble_hid
        if self.hw_config.split:
            if self.hw_config.split_ble:
                self.use_ble = True
            if not manager.uart.is_host:
                self.hid_constructor = DummyHID
            self.matrix_event_queue_cls = UARTMatrixEventQueue
        if self.use_ble:
            self.ble_helper = manager.ble_helper

        self.key_matrix = KeyMatrix(
            row_pins=self.hw_config.row_pins,
            column_pins=self.hw_config.column_pins,
            interval=self.hw_config.scan_interval,
        )
        self.matrix_event_queue = self.matrix_event_queue_cls(self, self.key_matrix)
        self.combo_buffer = ComboBuffer(self, self.matrix_event_queue)
        self.key_event_queue = KeyEventQueue(self, self.combo_buffer)
        self.hid = self.hid_constructor()
        return self
