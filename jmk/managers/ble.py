from supervisor import ticks_ms, reload
import gc

from .base import BaseKeyboardManager
from jmk.utils import ticks_add, ticks_less, ticks_diff, deep_sleep
from jmk import config
from jmk.led_helper import LEDHelper
from . import manager_registrar

try:
    from typing import TYPE_CHECKING, Optional
except ImportError:
    TYPE_CHECKING = None
    Optional = None
if TYPE_CHECKING:
    from jmk.keyboard import Keyboard
    from jmk.ble.base import BLEHelperBase
    from jmk.split.uart import UART


_1_MILLISECOND = 1  # I know this is obvious. Here for readability
_1_SECOND = 1000 * _1_MILLISECOND
_1_MINUTE = 60 * _1_SECOND


class BLEKeyboardManager(BaseKeyboardManager):
    """Manager with handling for BLE"""
    DEFAULT_TTL = 500 * _1_SECOND
    NOTIFY_INTERVAL = _1_MINUTE
    ACTIVITY_TIMEOUT = 45 * _1_MINUTE

    COMM_TTL = 5 * _1_SECOND
    QUICK_STATUS_INTERVAL = _1_SECOND
    STATUS_INTERVAL = _1_MINUTE

    def __init__(self, hw_config_module):
        self.hw_config = config.get_hw_config(hw_config_module)
        self.debug = self.hw_config.debug
        self.debug_keys = self.hw_config.debug_keys

        self.key_config = None
        self.led_helper: Optional[LEDHelper] = LEDHelper()
        self.keyboard: Optional[Keyboard] = None
        self.ble_helper: Optional[BLEHelperBase] = None
        self.uart: Optional[UART] = None
        self.is_host = True

        self.now: int = ticks_ms()
        self.now_s: int = self.now
        self._last_activity: int = self.now
        self._sleep_deadline: int = ticks_add(self.now, 300000)
        self._last_perf: int = self.now
        self._last_notify: int = self.now
        self._comm_deadline: int = self.now
        self._status_deadline: int = self.now
        self._quick_status_deadline: int = self.now
        self.last_status: str = ""
        self.performance: str = "CALCULATING"

        self._perf_counter: int = 0
        self.hz: float = 4
        self.loop_length = 2

        self.do_begin: bool = True
        self.do_tasks: bool = False

        self.register_manager()

    def begin(self, key_config_module) -> None:
        """Run beginning tasks, such as connecting BLE"""
        self.keep_awake()
        self.led_helper.init_red(self.hw_config.red_pin)
        self.led_helper.init_green(self.hw_config.green_pin)
        self.led_helper.init_blue(self.hw_config.blue_pin)
        while self._begin():
            pass
        self.key_config = config.get_key_config(key_config_module)

    def _begin(self) -> bool:
        if not self.do_begin:
            self.check_connections()  # BLE HID might not be on yet
            return True  # break the loop
        self.maybe_sleep()
        self.led_helper.update()
        self.maybe_notify()
        for _ in range(self.loop_length):
            if self.connect():
                self.do_begin = False
            self._perf_counter += 1
        self.evaluate_perf()

    def run(self) -> None:
        """Main loop of the program. Should be called after begin"""
        from jmk.keyboard import Keyboard
        self.keyboard = Keyboard(self.hw_config, self.key_config)
        self.keyboard.build()
        print("\nRunning main loop")
        self.led_helper.green_on()
        self.keep_awake()
        while True:
            self.now_s = ticks_ms()
            self.maybe_sleep()
            self.maybe_check_connections()
            self.maybe_notify()
            self.maybe_print_status()
            self.led_helper.update()
            for _ in range(self.loop_length):
                self._run()
            self.evaluate_perf()

    def _run(self) -> None:
        self.now: int = ticks_ms()
        if self.do_tasks:
            self.keyboard.update()
        self._perf_counter += 1

    def register_manager(self) -> None:
        print("\nRegistering manager")
        manager_registrar.register(self)

    @property
    def time_to_sleep(self) -> int:
        """How many ms until self.deep_sleep() should be run"""
        return ticks_diff(self._sleep_deadline, self.now_s)

    def log_activity(self) -> None:
        self._last_activity = self.now_s
        self.keep_awake(self.ACTIVITY_TIMEOUT)

    def keep_awake(self, ttl_ms: int = DEFAULT_TTL, force=False) -> None:
        """Refresh the sleep timer"""
        new_deadline = ticks_add(self.now_s, ttl_ms)
        if force:
            self._sleep_deadline = new_deadline
            return
        if ticks_less(self._sleep_deadline, new_deadline):
            self._sleep_deadline = new_deadline

    def maybe_sleep(self) -> None:
        """Sleep if time is up"""
        if ticks_less(self._sleep_deadline, self.now_s):
            self.deep_sleep()

    def deep_sleep(self) -> None:
        """Go to deep sleep, with pin alarm"""
        if self.keyboard.matrix_event_queue:
            self.keyboard.matrix_event_queue.primary_matrix.deinit()
        deep_sleep(self.keyboard.hw_config.column_pins, self.keyboard.hw_config.row_pins)

    def reload(self) -> None:
        """End and clear the program and restart"""
        print(f"\nSOFT RELOAD BY USER")
        if self.keyboard.key_matrix:
            self.keyboard.key_matrix.deinit()
        self.keyboard.hid.update([])
        reload()

    def evaluate_perf(self) -> None:
        """How many tight loops have run divided by the last time checked"""
        now = ticks_ms()
        ticks_since_perf = ticks_diff(now, self._last_perf)
        self._last_perf = now
        self.hz = (self._perf_counter/ticks_since_perf) * 1000
        self.loop_length = int(self.hz / 2) or 10  # break out of the tight loop about 2 times a second
        self._perf_counter = 0

    def maybe_notify(self) -> bool:
        """Send a notification message after an interval of time"""
        time_since_notify = ticks_diff(self.now_s, self._last_notify)
        if ticks_less(self.NOTIFY_INTERVAL, time_since_notify):
            self.notify()
            return True

    def notify(self):
        mem_free = gc.mem_free()
        self.performance = f"\nPerformance: {self.hz} Hz, Memory Free: {mem_free}"
        print(self.performance)
        print(f"Sleeping in {self.time_to_sleep / 1000} seconds")
        self._last_notify = self.now_s
        gc.collect()

    def maybe_print_status(self) -> bool:
        if self.ble_helper.status != self.last_status:
            print(f"\n{self.ble_helper.status}")
            self.last_status = self.ble_helper.status
            return True

        if ticks_less(self._quick_status_deadline, self.now_s):
            activity_deadline = ticks_add(self._last_activity, 5000)
            if ticks_less(activity_deadline, self.now_s):
                if ticks_less(self._status_deadline, self.now_s):
                    print(f"\nCONNECTION STATUS: NO CHANGE - {self.ble_helper.status}")
                    self._status_deadline = ticks_add(self.now_s, self.STATUS_INTERVAL)
                else:
                    print(".", end="")

            self._quick_status_deadline = ticks_add(self.now_s, self.QUICK_STATUS_INTERVAL)
            return True

    def maybe_check_connections(self) -> bool:
        if not self.ble_helper:
            return False
        if ticks_less(self._comm_deadline, self.now_s):
            if self.check_connections() and not self.led_helper.blue_io.value:
                self.led_helper.blue_off()
                return True

    def check_connections(self) -> bool:
        connected = self.ble_helper.check_connections()
        if not connected:
            self.led_helper.blue_blink()
            connected = self.connect()
        if connected:
            self.led_helper.blue_off()
            self.update_comm_deadline()
        self.do_tasks = connected
        return connected

    def update_comm_deadline(self, ttl_ms: int = COMM_TTL) -> None:
        self._comm_deadline = ticks_add(self.now_s, ttl_ms)

    def connect(self) -> bool:
        if ticks_less(self.DEFAULT_TTL, self.time_to_sleep):
            self.led_helper.blue_blink()
            self.keep_awake(self.DEFAULT_TTL, force=True)  # shorten sleep deadline if not connected

        if self.ble_helper.connect():
            self.update_comm_deadline()
            self.led_helper.blue_off()
            self.keep_awake()
            return True
        return False
