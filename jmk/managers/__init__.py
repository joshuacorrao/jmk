from .base import BaseKeyboardManager


class ManagerRegistrar:
    manager = None

    def register(self, _manager: BaseKeyboardManager):
        self.manager = _manager


manager_registrar = ManagerRegistrar()


def raise_import_error():
    raise ImportError("Manager was not imported properly, it must be the first import in this program")


def get_manager() -> BaseKeyboardManager:
    manager = manager_registrar.manager
    if manager is None:
        raise_import_error()
    return manager_registrar.manager
