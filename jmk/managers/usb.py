from supervisor import ticks_ms, reload
import gc

from .base import BaseKeyboardManager
from jmk.utils import ticks_add, ticks_less, ticks_diff
from jmk import config
from jmk.led_helper import LEDHelper
from . import manager_registrar

try:
    from typing import TYPE_CHECKING, Optional
except ImportError:
    TYPE_CHECKING = None
    Optional = None
if TYPE_CHECKING:
    from jmk.keyboard import Keyboard


_1_MILLISECOND = 1  # I know this is obvious. Here for readability
_1_SECOND = 1000 * _1_MILLISECOND
_1_MINUTE = 60 * _1_SECOND


class USBKeyboardManager(BaseKeyboardManager):
    """Manager with handling for BLE"""
    DEFAULT_TTL = 500 * _1_SECOND
    NOTIFY_INTERVAL = _1_MINUTE

    QUICK_STATUS_INTERVAL = _1_SECOND
    STATUS_INTERVAL = _1_MINUTE

    def __init__(self, hw_config_module):
        self.hw_config = config.get_hw_config(hw_config_module)
        self.debug = self.hw_config.debug
        self.debug_keys = self.hw_config.debug_keys

        self.key_config = None
        self.led_helper: Optional[LEDHelper] = LEDHelper()
        self.keyboard: Optional[Keyboard] = None
        self.is_host = True

        self.now: int = ticks_ms()
        self.now_s: int = self.now
        self._last_activity: int = self.now
        self._sleep_deadline: int = ticks_add(self.now, 300000)
        self._last_perf: int = self.now
        self._last_notify: int = self.now
        self._status_deadline: int = self.now
        self._quick_status_deadline: int = self.now
        self.last_status: str = ""
        self.performance: str = "CALCULATING"

        self._perf_counter: int = 0
        self.hz: float = 4
        self.loop_length = 2

        self.do_begin: bool = True
        self.do_tasks: bool = False

        self.register_manager()

    def begin(self, key_config_module) -> None:
        """Run beginning tasks, such as connecting BLE"""
        self.led_helper.init_red(self.hw_config.red_pin)
        self.led_helper.init_green(self.hw_config.green_pin)
        self.led_helper.init_blue(self.hw_config.blue_pin)
        while self._begin():
            pass
        self.key_config = config.get_key_config(key_config_module)

    def _begin(self) -> bool:
        """Pure USB requires no startup tasks, return True immediately"""
        return True

    def run(self) -> None:
        """Main loop of the program. Should be called after begin"""
        from jmk.keyboard import Keyboard
        self.keyboard = Keyboard(self.hw_config, self.key_config)
        self.keyboard.build()
        print("\nRunning main loop")
        self.led_helper.green_on()
        while True:
            self.now_s = ticks_ms()
            self.maybe_notify()
            self.maybe_print_status()
            self.led_helper.update()
            for _ in range(self.loop_length):
                self._run()
            self.evaluate_perf()

    def _run(self) -> None:
        self.now: int = ticks_ms()
        if self.do_tasks:
            self.keyboard.update()
        self._perf_counter += 1

    def register_manager(self) -> None:
        print("Registering Manager")
        manager_registrar.register(self)

    def log_activity(self) -> None:
        self._last_activity = self.now_s

    def reload(self) -> None:
        """End and clear the program and restart"""
        print(f"\nSOFT RELOAD BY USER")
        if self.keyboard.key_matrix:
            self.keyboard.key_matrix.deinit()
        self.keyboard.hid.update([])
        reload()

    def evaluate_perf(self) -> None:
        """How many tight loops have run divided by the last time checked"""
        now = ticks_ms()
        ticks_since_perf = ticks_diff(now, self._last_perf)
        self._last_perf = now
        self.hz = (self._perf_counter/ticks_since_perf) * 1000
        self.loop_length = int(self.hz / 2) or 10  # break out of the tight loop about 2 times a second
        self._perf_counter = 0

    def maybe_notify(self) -> bool:
        """Send a notification message after an interval of time"""
        time_since_notify = ticks_diff(self.now_s, self._last_notify)
        if ticks_less(self.NOTIFY_INTERVAL, time_since_notify):
            self.notify()
            return True

    def notify(self):
        mem_free = gc.mem_free()
        self.performance = f"\nPerformance: {self.hz} Hz, Memory Free: {mem_free}"
        print(self.performance)
        self._last_notify = self.now_s
        gc.collect()
