try:
    from typing import TYPE_CHECKING, Optional
except ImportError:
    TYPE_CHECKING = None
    Optional = None
if TYPE_CHECKING:
    from jmk.led_helper import LEDHelper
    from jmk.keyboard import Keyboard
    from jmk.ble.base import BLEHelperBase
    from jmk.split.uart import UART
    from jmk.config import HardwareConfig, KeyConfig


class BaseKeyboardManager:
    """Main program control flow, Registers all main operation objects, holds functions to manage all"""
    debug: bool
    debug_keys: bool

    DEFAULT_TTL: int
    NOTIFY_INTERVAL: int
    ACTIVITY_TIMEOUT: int

    # unused if no connections need to be maintained
    COMM_TTL: int
    QUICK_STATUS_INTERVAL: int
    STATUS_INTERVAL: int

    hw_config: HardwareConfig
    key_config: KeyConfig
    led_helper: Optional[LEDHelper]
    keyboard: Optional[Keyboard]
    ble_helper: Optional[BLEHelperBase]
    uart: UART
    is_host: bool

    now: int
    _last_activity: int
    _sleep_deadline: int
    _last_notify: int
    _comm_deadline: int
    _status_deadline: int
    _quick_status_deadline: int
    last_status: str
    performance: str

    _perf_counter: int
    hz: float

    do_begin: bool
    do_tasks: bool

    def begin(self, key_config_module) -> None:
        raise NotImplementedError

    def _begin(self) -> bool:
        raise NotImplementedError

    def run(self) -> None:
        raise NotImplementedError

    def _run(self) -> None:
        raise NotImplementedError

    def register_manager(self) -> None:
        raise NotImplementedError

    @property
    def time_to_sleep(self) -> int:
        raise NotImplementedError

    def log_activity(self) -> None:
        raise NotImplementedError

    def keep_awake(self, ttl_ms: int, force: bool) -> None:
        raise NotImplementedError

    def maybe_sleep(self) -> None:
        raise NotImplementedError

    def deep_sleep(self) -> None:
        raise NotImplementedError

    @staticmethod
    def reload() -> None:
        raise NotImplementedError

    def evaluate_perf(self) -> float:
        raise NotImplementedError

    def maybe_notify(self) -> bool:
        raise NotImplementedError

    def notify(self) -> None:
        raise NotImplementedError

    def maybe_print_status(self) -> bool:
        raise NotImplementedError

    def maybe_check_connections(self) -> bool:
        raise NotImplementedError

    def check_connections(self) -> bool:
        raise NotImplementedError

    def update_comm_deadline(self, ttl_ms: int) -> None:
        raise NotImplementedError

    def connect(self) -> bool:
        raise NotImplementedError
