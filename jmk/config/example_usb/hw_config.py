import board


DEBUG_OUTPUT = True
DEBUG_KEYS = True
SCAN_INTERVAL_MS = 20  # How often the key matrix scanner circuit checks for key switch state

HID_BLE = False
SPLIT = False
SPLIT_BLE = False

RED_PIN = board.LED_RED
GREEN_PIN = board.LED_GREEN
BLUE_PIN = board.LED_BLUE

ROW_PINS = (board.D10, board.D9, board.D7, board.D8)
COL_PINS = (board.D2, board.D1, board.D6, board.D5, board.D4, board.D3, board.D0)

COORDS = (
    0,  1,  2,  3,  4,  5,  6,
    7,  8,  9,  10, 11, 12, 13,
    14, 15, 16, 17, 18, 19,     20,
            23, 24, 25, 26, 27,     22,
                                21,
)
