import jmk.k as k
from jmk.keys import Combo

from .custom import LAY1


COMBOS = {
    Combo(k.LSFT, (27, 8)),
    Combo(k.LCTL, (27, 9)),
    Combo(k.LALT, (27, 10)),
    Combo(k.LCMD, (27, 11)),

    Combo(k.L_SHIFT_CTRL, (27, 8, 9)),
    Combo(k.L_SHIFT_ALT, (27, 8, 10)),
    Combo(k.L_SHIFT_CMD, (27, 8, 11)),
    Combo(k.L_CTRL_ALT, (27, 9, 10)),
    Combo(k.L_CTRL_CMD, (27, 9, 11)),
    Combo(k.L_ALT_CMD, (27, 10, 11)),

    Combo(k.L_SHIFT_CTRL_ALT, (27, 8, 9, 10)),
    Combo(k.L_SHIFT_CTRL_CMD, (27, 8, 9, 11)),
    Combo(k.L_SHIFT_ALT_CMD, (27, 8, 10, 11)),
    Combo(k.L_CTRL_ALT_CMD, (27, 9, 10, 11)),
}

KEYS = (
    (
        k.TAB,  k._Q,   k._W,   k._E,   k._R,   k._T,   k.NULL,
        k.ESC,  k._A,   k._S,   k._D,   k._F,   k._G,   LAY1,
        k.LSFT, k._Z,   k._X,   k._C,   k._V,   k._B,           k.NULL,
                        k.LCTL, k.LCMD, k.LFT,  k.RGT,  k.SPC,          k.NULL,
                                                                k.BSP,
    ),
    (
        k.LCBR, k.RCBR, k.LPRN, k.LBRC, k.RBRC, k.RPRN, k.NULL,
        k.ESC,  k._1,   k._2,   k._3,   k._4,   k._5,   k.NULL,
        k.LSFT, k.ECLM, k.AT,   k.HASH, k.DLLR, k.PERC,         k.NULL,
                        k.LCTL, k.LCMD, k.LFT,  k.HOME, k.END,          k.NULL,
                                                                k.BSP,
    ),
)
