"""
Example if you took the left half of my split wireless keyboard
and plugged it in directly to use and didn't use the right half
USB only
"""


def start_keyboard():
    # Always import and init the manager first
    from jmk.managers.usb import USBKeyboardManager
    from . import hw_config
    keyboard_manager = USBKeyboardManager(hw_config)

    from . import key_config
    keyboard_manager.begin(key_config)
    keyboard_manager.run()
