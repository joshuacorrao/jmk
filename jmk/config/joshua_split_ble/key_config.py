import jmk.k as k
from jmk.keys import Combo
import jmk.config.joshua_split_ble.custom as j


COMBOS = {
    Combo(k.LSFT, (27, 8)),
    Combo(k.LCTL, (27, 9)),
    Combo(k.LALT, (27, 10)),
    Combo(k.LCMD, (27, 11)),

    Combo(k.L_SHIFT_CTRL, (27, 8, 9)),
    Combo(k.L_SHIFT_ALT, (27, 8, 10)),
    Combo(k.L_SHIFT_CMD, (27, 8, 11)),
    Combo(k.L_CTRL_ALT, (27, 9, 10)),
    Combo(k.L_CTRL_CMD, (27, 9, 11)),
    Combo(k.L_ALT_CMD, (27, 10, 11)),

    Combo(k.L_SHIFT_CTRL_ALT, (27, 8, 9, 10)),
    Combo(k.L_SHIFT_CTRL_CMD, (27, 8, 9, 11)),
    Combo(k.L_SHIFT_ALT_CMD, (27, 8, 10, 11)),
    Combo(k.L_CTRL_ALT_CMD, (27, 9, 10, 11)),

    Combo(k.RCMD, (55, 39)),
    Combo(k.RALT, (55, 38)),
    Combo(k.RCTL, (55, 37)),
    Combo(k.RSFT, (55, 36)),

    Combo(k.L_SHIFT_CTRL, (55, 36, 37)),
    Combo(k.L_SHIFT_ALT, (55, 36, 38)),
    Combo(k.L_SHIFT_CMD, (55, 36, 39)),
    Combo(k.L_CTRL_ALT, (55, 37, 38)),
    Combo(k.L_CTRL_CMD, (55, 37, 39)),
    Combo(k.L_ALT_CMD, (55, 38, 39)),

    Combo(k.L_SHIFT_CTRL_ALT, (55, 36, 37, 38)),
    Combo(k.L_SHIFT_CTRL_CMD, (55, 36, 37, 39)),
    Combo(k.L_SHIFT_ALT_CMD, (55, 36, 38, 39)),
    Combo(k.L_CTRL_ALT_CMD, (55, 37, 38, 39)),
}


KEYS = (
    # LAYER 0
    # noqa
    (
        k.TAB,  k._q,   k._w,   k._e,   k._r,   k._t,   k.RBRC,
        k.ESC,  k._a,   k._s,   k._d,   k._f,   k._g,   j.LAY1,
        k.LSFT, k._z,   k._x,   k._c,   k._v,   k._b,           k.MINS,
                        k.LCMD, k.LFT,  k.RGT,  j.LAY2, j.SPL1,         j.TO1,
                                                                k.BSP,

                        k.LBRC, k._y,   k._u,   k._i,   k._o,   k._p,   k.BSLS,
                        j.LAY1, k._h,   k._j,   k._k,   k._l,   k.SCLN, k.SQUO,
                k.EQL,          k._n,   k._m,   k.COMM, k.DOT,  k.SLSH, k.RSFT,
        k.NULL,         j.SPL1, j.LAY2, k.DOWN, k.UP,   k.RCMD,
                k.ENT,
    ),
    # LAYER 1 Symbols
    (
        k.LCBR, k.ECLM, k.AT,   k.HASH, k.DLLR, k.PERC, k.NULL,
        k.ESC,  k.LSFT, k.LCTL, k.LALT, k.LCMD, k.HYPR, j.LAY1,
        k.LSFT, k.NULL, k.LCBR, k.RCBR, k.LBRC, k.RBRC,         k.NULL,
                        k.LCMD, k.HOME, k.END,  j.LAY2, k.SPC,          j.TO2,
                                                                k.DEL,

                        k.NULL, k.CIRC, k.AMPR, k.ASTR, k.LPRN, k.RPRN, k.TILD,
                        j.LAY1, k.HYPR, k.RCMD, k.RALT, k.RCTL, k.RSFT, k.GRAV,
                k.NULL,         k.EQL,  k.UNDS, k.MINS, k.PLUS, k.NULL, k.RSFT,
        j.TO0,          k.SPC,  j.LAY2, k.PGDN, k.PGUP, k.RCMD,
                k.ENT,
    ),
    # LAYER 2 Numbers and F keys
    (
        k.NULL, k._1,   k._2,   k._3,   k._4,   k._5,   k.NULL,
        k.ESC,  k.LSFT, k.LCTL, k.LALT, k.LCMD, k.HYPR, j.LAY1,
        k.LSFT, k.F1,   k.F2,   k.F3,   k.F4,   k.F5,           k.F11,
                        k.LCMD, k.HOME, k.END,  j.LAY2, k.SPC,          k.NULL,
                                                                k.DEL,

                        k.NULL, k._6,   k._7,   k._8,   k._9,   k._0,   k.NULL,
                        j.LAY1, k.HYPR, k.RCMD, k.RALT, k.RCTL, k.RSFT, k.NULL,
                k.F12,          k.F6,   k.F7,   k.F8,   k.F9,   k.F10,  k.RSFT,
        j.TO1,          k.SPC,  j.LAY2, k.PGDN, k.PGUP, k.RCMD,
                k.ENT,
    ),
    # LAYER 3 Currently unused
    (
        k.NULL, k.NULL, k.NULL, k.NULL, k.NULL, k.NULL, k.NULL,
        k.NULL, k.NULL, k.NULL, k.NULL, k.NULL, k.NULL, k.NULL,
        k.NULL, k.NULL, k.NULL, k.NULL, k.NULL, k.NULL,         k.NULL,
                        k.NULL, k.NULL, k.NULL, k.NULL, k.NULL,         k.NULL,
                                                                k.NULL,

                        k.NULL, k.NULL, k.NULL, k.NULL, k.NULL, k.NULL, k.NULL,
                        k.NULL, k.NULL, k.NULL, k.NULL, k.NULL, k.NULL, k.NULL,
                k.NULL,         k.NULL, k.NULL, k.NULL, k.NULL, k.NULL, k.NULL,
        j.TO2,          k.NULL, k.NULL, k.NULL, k.NULL, k.NULL,
                k.NULL,
    ),
)
