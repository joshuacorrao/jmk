# from jmk.manager import manager
from jmk.keys import (
    LayerHoldKey,
    LayerHoldTapKey,
    # FunctionKey,
    # SequenceKey,
    # HoldTapKey,
    # OneShotHoldKey,
)
import jmk.k as k
# from jmk.queues.key_event import KeyEvent

# try:
#     from typing import TYPE_CHECKING
# except ImportError:
#     TYPE_CHECKING = None
# if TYPE_CHECKING:
#     from keypad import Event as MatrixEvent
#     from jmk.keys import Key
#     from jmk.keyboard import Keyboard


# def restart_keybord(key: Key, keyboard: Keyboard, matrix_event: MatrixEvent):
#     if matrix_event.pressed:
#         manager.reload()
#
#
# def print_perf(key, keyboard: Keyboard, matrix_event):
#     if matrix_event.pressed:
#         manager.notify()
#         sequence = manager.performance or "No performance recorded yet"
#         new_key = Sequence(sequence, "<PERF>")
#         ke = KeyEvent(new_key, matrix_event)
#         keyboard.key_event_queue.buffer_1.append(ke)


# def clear_bonding(key, keyboard, matrix_event):
#     manager.ble_helper.clear_bonding()
#
#
# def togg_debug(key, keyboard, matrix_event):
#     if matrix_event.pressed:
#         print("TOGGLE DEBUG")
#         manager.debug = not manager.debug
#         print("TOGGLE DEBUG - Done")


# RELD = FunctionKey(restart_keybord, deferrable=False)
# PERF = FunctionKey(print_perf, deferrable=False)
# CLRB = FunctionKey(clear_bonding, deferrable=False)
# DBG = FunctionKey(togg_debug, deferrable=False)


# help i got stuck in a different layer - key
LAY0 = LayerHoldKey(0, 0, "<LAY 0>")
LAY1 = LayerHoldKey(1, 0, "<LAY 1>")
LAY2 = LayerHoldKey(2, 0, "<LAY 2>")
# LAY3 = LayerHoldKey(3, 0, "<LAY 3>")

TO0 = LAY0
TO1 = LayerHoldKey(1, 1, "<TO 1>")
TO2 = LayerHoldKey(2, 2, "<TO 2>")
# TO3 = LayerHoldKey(3, 3, "<TO 3>")
# hold to temp switch to layer 1. Tap for spacebar
# SPL1 = HoldTapKey(LAY1, k.SPC, "<HT SPACE LAY1>")
SPL1 = LayerHoldTapKey(1, 0, k.SPC, "<LHT 1>")
# BSL2 = HoldTapKey(LAY2, k.BSP, "<HT BACKSPACE LAY2>")
# DEL2 = HoldTapKey(LAY2, k.DEL, "<HT DELETE LAY2>")

# examples of others
# Tap holds Shift until the next key press releases
# OSSH = OneShotHoldKey(hold_key=k.LSFT, label="<OS LSHFT>")

# foo_seq = Sequence(
#     "The quick brown fox jumps over the lazy dog!\nTesting return.", "foo")
# zen_seq = Sequence(
#     "Beautiful is better than ugly.\n"
#     "Explicit is better than implicit.\n"
#     "Simple is better than complex.\n"
#     "Complex is better than complicated.\n"
#     "Flat is better than nested.\n"
#     "Sparse is better than dense.\n"
#     "Readability counts.\n"
#     "Special cases aren't special enough to break the rules.\n"
#     "Although practicality beats purity.\n"
#     "Errors should never pass silently.\n"
#     "Unless explicitly silenced.\n"
#     "In the face of ambiguity, refuse the temptation to guess.\n"
#     "There should be one-- and preferably only one --obvious way to do it.\n"
#     "Although that way may not be obvious at first unless you're Dutch.\n"
#     "Now is better than never.\n"
#     "Although never is often better than *right* now.\n"
#     "If the implementation is hard to explain, it's a bad idea.\n"
#     "If the implementation is easy to explain, it may be a good idea.\n"
#     "Namespaces are one honking great idea -- let's do more of those!\n",
#     "ZEN"
# )
