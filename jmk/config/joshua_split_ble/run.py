def start_keyboard():
    # Always import and init the manager first
    from jmk.managers.ble import BLEKeyboardManager
    import jmk.config.joshua_split_ble.hw_config as hw_config
    keyboard_manager = BLEKeyboardManager(hw_config)

    # BLE should be imported second if needed
    # WIP, only have split config as an example
    if keyboard_manager.hw_config.split:
        from jmk.ble import create_split_ble
        ble_helper = create_split_ble()

    # now import the bigger stuff
    import jmk.config.joshua_split_ble.key_config as key_config
    keyboard_manager.begin(key_config)
    keyboard_manager.run()
