# Configuration README

## hw_config.py
- define your scan interval `SCAN_INTERVAL_MS`, a value of `20` should be safe, `5` can work; beware of key bouncing
- define your rows `ROW_PINS` as a tuple of `Microcontroller.Pin` objects (usually from the `board` module)
- define your columns `COL_PINS` as a tuple of `Microcontroller.Pin` objects
- define your coordinates map `COORDS` as a tuple of `int`'s counting 0 to `key count - 1` that best represents the way that keys on the keyboard are laid out
  - On a 3x3 key matrix: 
    - row pin 0, col pin 0 is key #0
    - row pin 0, col pin 1 is key #1
    - row pin 1, col pin 0 is key #3, etc...
  - Sometimes the key order doesn't match the physical order due to how the keys are actually wired, so keep that in mind
  - Adding new lines and indents to this tuple can help visually organize the coordinates
  - If this is a split keyboard, count up to `key count - 1` where "key_count" is the number of keys on both halves
  - If the second half is symmetrically wired, counting each row backwards for the second half's numbers can help for later
- If this is a split keyboard define `OFFSET` as the number of keys on the master side of the keyboard
  - typically both sides have the same number of keys so it just the length of `COORDS` / 2
- `LED_RED`, `LED_GREEN`, `LED_BLUE` can be defined as well to be used for led functions later

## key_config.py
- define `KEYS` as a tuple of tuples
  - 1 tuple for each key layer you want to be able to switch to. 
  - Each inner tuple is a tuple of Key (and Key subclass) objects listed in the exact same order as `COORDS` above
  - Pre-defined key objects can be imported from `jmk.k`, see example_usb config for an example
- define `COMBOS` as a set() of `Combo` objects

## run.py
### WIP, Follow template below or the one in example_usb/run.py


```python
def start_keyboard():
    # Always import and init the manager first
    from jmk.managers.ble import BLEKeyboardManager
    import jmk.config.joshua_split_ble.hw_config as hw_config
    keyboard_manager = BLEKeyboardManager(hw_config)

    # BLE should be imported second if needed
    # WIP, only have split config as an example
    if keyboard_manager.hw_config.split:
        from jmk.ble import create_split_ble
        ble_helper = create_split_ble()

    # now import the bigger stuff and start
    import jmk.config.joshua_split_ble.key_config as key_config
    keyboard_manager.begin(key_config)
    keyboard_manager.run()

```
