try:
    from typing import TYPE_CHECKING
except ImportError:
    TYPE_CHECKING = None
if TYPE_CHECKING:
    from jmk.keys import Key
    from jmk.queues.combo_buffer import Combo


class HardwareConfig:

    def __init__(
            self,
            scan_interval: float,
            row_pins,
            column_pins,
            red_pin,
            green_pin,
            blue_pin,
            coords: tuple[int],
            offset: int,
            hid_ble: bool,
            split: bool,
            split_ble: bool,
            debug: bool,
            debug_keys: bool,
    ):
        self.scan_interval: float = scan_interval
        self.row_pins = row_pins
        self.column_pins = column_pins
        self.red_pin = red_pin
        self.green_pin = green_pin
        self.blue_pin = blue_pin
        self.coords: tuple[int] = coords
        self.inverse_coords: tuple[int] = tuple(self.coords.index(x) for x in range(len(self.coords)))
        self.hid_ble: bool = hid_ble
        self.split: bool = split
        self.split_ble: bool = split_ble
        self.matrix_size = len(row_pins) * len(column_pins)
        self.offset: int = offset
        self.debug: bool = debug
        self.debug_keys: bool = debug_keys


def get_hw_config(config_module):
    row_pins = config_module.ROW_PINS
    col_pins = config_module.COL_PINS
    coords = config_module.COORDS

    scan_interval_ms = getattr(config_module, "SCAN_INTERVAL_MS", 20)
    scan_interval = scan_interval_ms / 1000

    red_pin = getattr(config_module, "RED_PIN", None)
    green_pin = getattr(config_module, "GREEN_PIN", None)
    blue_pin = getattr(config_module, "BLUE_PIN", None)

    hid_ble = getattr(config_module, "HID_BLE", False)
    split = getattr(config_module, "SPLIT", False)
    split_ble = getattr(config_module, "SPLIT_BLE", False)
    offset = getattr(config_module, "OFFSET", 0)

    debug = getattr(config_module, "DEBUG_OUTPUT", False)
    debug_keys = getattr(config_module, "DEBUG_KEYS", False)

    hardware_config = HardwareConfig(
        scan_interval=scan_interval,
        row_pins=row_pins,
        column_pins=col_pins,
        red_pin=red_pin,
        green_pin=green_pin,
        blue_pin=blue_pin,
        coords=coords,
        hid_ble=hid_ble,
        split=split,
        split_ble=split_ble,
        offset=offset,
        debug=debug,
        debug_keys=debug_keys,
    )
    return hardware_config


class KeyConfig:
    keys: tuple[Key]
    combos: set[Combo]
    layers: tuple[tuple[Key]]

    def __init__(self, keys: tuple[Key], combos: set[Combo], layers: tuple[tuple[Key]]):
        self.keys = keys
        self.combos = combos
        self.layers = layers


def get_key_config(config_module):
    keys = config_module.KEYS[0]
    combos = config_module.COMBOS
    layers = config_module.KEYS
    key_config = KeyConfig(keys=keys, combos=combos, layers=layers)
    return key_config
