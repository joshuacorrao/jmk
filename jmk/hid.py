import usb_hid
from adafruit_ble.services.standard.hid import HIDService

from jmk.manager import manager

try:
    from typing import TYPE_CHECKING
except ImportError:
    TYPE_CHECKING = None
if TYPE_CHECKING:
    import usb_hid
    from jmk.keys import Key


class HIDUsage:
    KEYBOARD = 0x06
    MOUSE = 0x02
    CONSUMER = 0x01
    SYSCONTROL = 0x80


class HIDUsagePage:
    CONSUMER = 0x0C
    KEYBOARD = MOUSE = SYSCONTROL = 0x01


class KeyboardHID:
    """Turn pressed keys into reports and send them"""
    empty_bytes = bytearray(8)

    def __init__(self, hid: usb_hid):
        self.hid_service = hid
        for device in self.hid_service.devices:
            if (device.usage_page, device.usage) == (HIDUsagePage.KEYBOARD, HIDUsage.KEYBOARD):
                self.device = device
        self.last_report_bytes = bytearray(8)
        self.report_bytes = bytearray(8)  # 6kro
        self.pressed_mods: set[int] = set()
        self.mod_value = 0

    def build_report(self, pressed_keys: list[Key]):
        # clear codes first
        self.pressed_mods.clear()
        self.report_bytes[:] = self.empty_bytes
        self.report_key_codes(pressed_keys)
        self.report_mod_codes()

    def report_mod_codes(self):
        mod_code = 0
        for code in self.pressed_mods:
            mod_code |= code
        self.mod_value = mod_code
        self.report_bytes[0] = mod_code

    def report_key_codes(self, pressed_keys: list[Key]):
        for key in pressed_keys:

            if key.modifiers:  # add its modifiers first
                self.pressed_mods.update(key.modifiers)

            code_byte = key.code.to_bytes(1, "little")
            if code_byte in self.report_bytes:
                continue  # skip if it's already in there

            # find first available byte and assign code to it
            idx = self.report_bytes.find(b'\x00', 2)
            if idx == -1:
                continue  # no more room for regular key codes

            self.report_bytes[idx] = key.code  # adding code to bytes at position idx

    def send_report(self):
        if self.report_bytes == self.last_report_bytes:
            return
        if manager.debug and manager.debug_keys:
            print(f"{{{self.mod_value:>02x}}}", end=" ")
            # ^^ print the current mod code for every report sent

        self.device.send_report(self.report_bytes)
        self.last_report_bytes[:] = self.report_bytes

    def update(self, pressed_keys: list[Key]):
        self.build_report(pressed_keys)
        self.send_report()

    @classmethod
    def get_usb_hid(cls):
        return cls(usb_hid)

    @classmethod
    def get_ble_hid(cls):
        hid_service = HIDService()
        return cls(hid_service)

    @classmethod
    def get_dummy_hid(cls):
        return DummyHID()


class DummyHID:
    def update(self, pressed_keys):
        pass
