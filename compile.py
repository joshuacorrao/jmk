


import os
import glob

compiler = "./mpy-cross"
SRC_DIR = "./jmk/"
MPY_DIR = "./mpy/jmk/"

source_dirs = glob.glob('**/', root_dir=SRC_DIR, recursive=True)
dest_dirs = glob.glob('**/', root_dir=MPY_DIR, recursive=True)
missing_dirs = [dir for dir in source_dirs if dir not in dest_dirs]
had_errors = False
for dir in missing_dirs:
    new_dir = f"{MPY_DIR}/{dir}"
    mkdir_cmd = f"mkdir {new_dir}"
    code = os.system(mkdir_cmd)
    if code:
        had_errors = True
        print(mkdir_cmd)
        print(code)


file_names = glob.glob('**/*.py', root_dir=SRC_DIR, recursive=True)
for file_name in file_names:
    input_file = f"{SRC_DIR}/{file_name}"
    output_file = f"{MPY_DIR}/{file_name[:-2]}mpy"
    command = f"{compiler} {input_file} -o {output_file}"
    code = os.system(command)
    if code:
        had_errors = True
        print(command)
        print(code)

# command = f"cp ./src/code.py ./mpy/code.py"
# code = os.system(command)
# if code:
#     had_errors = True
#     print(command)
#     print(code)

if had_errors:
    print("errors while processing")
else:
    print("compile success")
